/**
 headstak - a stack for your head
 
 Copyright (C) 2012 dreadtech.com
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 **/
//
//  StakWindowController.h
//  headstak
//
//  Created by Veghead on 8/6/11.


#import <Cocoa/Cocoa.h>

@class StakView;
@class StakItem;
@class StakViewController;

@interface StakWindowController : NSWindowController {
    StakViewController *_controller;
    IBOutlet StakView *_stakView;
    IBOutlet NSTextField *_caption;
}

@property (strong, nonatomic) StakViewController *controller;
@property (strong, nonatomic) StakView *stakView;
@property (strong, nonatomic) NSTextField *caption;

- (BOOL)windowShouldClose:(id)sender;
- (void)layoutItemViews;
- (void)openStakWindow;
- (void)windowHiding;

@end
