/**
 headstak - a stack for your head
 
 Copyright (C) 2012 dreadtech.com
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 **/
//
//  StakDB.h
//  headstak
//
//  Created by Veghead on 4/1/12.


#import "FMDatabase.h"

@class StakView;

@interface StakDB : FMDatabase {
    NSDictionary *_itemTypes;
}

@property (strong, nonatomic) NSDictionary *itemTypes;

+ (id)stackDB;
+ (NSDate *)timestampFieldToDate:(NSNumber *)timestamp;
- (void)buildFreshStak;
- (NSString *)typeNameForId:(NSUInteger)typeId;
- (void)loadStakIntoHolder:(NSMutableArray *)holder;
- (void)hardDeleteStak;
- (void)dumpStakToFileHandle:(NSFileHandle *)handle;
@end
