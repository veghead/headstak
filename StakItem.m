/**
 headstak - a stack for your head
 
 Copyright (C) 2012 dreadtech.com
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 **/
//
//  StakItem.m
//  headstak
//
//  Created by Veghead on 3/11/12.


#import "StakItem.h"
#import "StakItemView.h"
#import "StakDB.h"
#import "StakApplication.h"


@implementation StakItem


- (id)initNewItemOfType:(NSUInteger)type inDb:(StakDB *)db {
    self = [super init];
    if (self) {
        _db = db;
        _view = nil;
        NSDate *now = [NSDate date];
        _pushTime = now;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeStyle:NSDateFormatterNoStyle];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        NSString *dateDesc = [NSDateFormatter localizedStringFromDate:_pushTime 
                                                            dateStyle:NSDateFormatterMediumStyle
                                                            timeStyle:NSDateFormatterMediumStyle];
        
        _itemType = type;
        NSString *itemTypeString = [db typeNameForId:_itemType];
        _itemDescription = [NSString stringWithFormat:@"%@ - %@\n", itemTypeString, dateDesc];
        
        [db executeUpdate:@"INSERT INTO stakitems\
         (pushdate, description, itemtype)\
         values (?,?,?)",now, _itemDescription,
         [NSNumber numberWithInteger:_itemType]];
        
        _itemId = [[self db] lastInsertRowId];
    }
    return self;

}

- (id)initWithDictionary:(NSDictionary *)dict inDb:(StakDB *)db {
    self = [super init];
    if (self) {
        _view = nil;
        _db = db;
        _pushTime = [StakDB timestampFieldToDate:[dict objectForKey:@"pushdate"]];
        _popTime = [StakDB timestampFieldToDate:[dict objectForKey:@"popdate"]];
        _itemType = [[dict objectForKey:@"itemtype"] integerValue];
        _itemDescription = [dict objectForKey:@"description"];
        _itemId = [[dict objectForKey:@"id"] integerValue];
    }
    return self;
}

/**
  * @brief get the view associated with this item
  */
- (StakItemView *)viewInStakView:(StakView *)stakView {
    if ([self view]) return [self view];
    [self setView:[[StakItemView alloc] initWithStakItem:self inStakView:stakView]];
    return [self view];
}

/**
  * @brief write the item back to the database
  */
- (void)save {
    [[self db] executeUpdate:@"UPDATE stakitems SET \
     pushdate = ?, popdate = ?, description = ?, itemtype = ? \
     WHERE id = ?", [self pushTime], [self popTime], [self itemDescription],
     [NSNumber numberWithInt:[self itemType]], [NSNumber numberWithInt:[self itemId]]];
}

/**
  * @brief what colour are we according to our itemType?
  */
- (NSColor *)colour {
    NSNumber *itemTypeId = [NSNumber numberWithInt:[self itemType]];
    NSDictionary *types = [[self db] itemTypes];
    NSDictionary *colours = [types objectForKey:@"colours"];
    NSString *colourString = [colours objectForKey:itemTypeId];
    return [StakApplication colourFromHexRGB:colourString];
}

@end
