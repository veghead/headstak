/**
    headstak - a stack for your head
     
    Copyright (C) 2012 dreadtech.com
     
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

**/

//
//  headstak_AppDelegate.h
//  headstak
//
//  Created by Veghead on 25/06/2011.

#import <Cocoa/Cocoa.h>
@class StakViewController;

@interface headstak_AppDelegate : NSObject 
{
    NSStatusItem *statusItem;
    IBOutlet NSMenu *_stakMenu;
}

enum {
    STAK_MENU_SHOW = 1,
    STAK_MENU_CLEAR_HIST = 2,
    STAK_MENU_EXPORT_HIST = 3,
    STAK_MENU_PREFS = 4,
    STAK_MENU_ABOUT = 5,
    STAK_MENU_QUIT = 6
};

@property (nonatomic, strong) IBOutlet NSWindow *window;
@property (nonatomic, strong) StakViewController *controller;
@property (strong, nonatomic) NSMenu *stakMenu;

- (IBAction)menuHandler:(id)sender;

@end
