//
//  StakPrefController.h
//  headstak
//
//  Created by Veghead on 6/7/12.
//  Copyright (c) 2012 dreadtech.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface StakPrefController : NSWindowController {

}

- (void)openStakWindow;
- (IBAction) close:(id)sender;

@property (strong) IBOutlet NSColorWell *color;
@property (strong) IBOutlet NSTextField *name;

@end
