/**
 headstak - a stack for your head
 
 Copyright (C) 2012 dreadtech.com
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 **/
//
//  HSApplication.h
//  headstak
//
//  Created by Veghead on 7/24/11.


#import <Foundation/Foundation.h>

#ifdef DEBUG
#define LogDebug( s, ... )
#else
#define LogDebug( s, ... ) NSLog( @"<%p %@:(%d)> %@", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#endif
#define LogInfo( s, ... ) NSLog( @"<%p %@:(%d)> [Info] %@", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#define LogError( s, ... ) NSLog( @"<%p %@:(%d)> [Error] %@", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )

@interface StakApplication : NSApplication {
}

@property (strong, readwrite) NSMutableArray *theStak;

- (IBAction)aboutBox:(id)sender;
+ (NSColor *)colourFromHexRGB:(NSString *) inColorString;

@end
