/**
 headstak - a stack for your head
 
 Copyright (C) 2012 dreadtech.com
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 **/
//
//  StakItem.h
//  headstak
//
//  Created by Veghead on 3/11/12.


#import <AppKit/AppKit.h>

@class StakView;
@class StakItem;

@interface StakItemView : NSBox <NSTextViewDelegate>
{
    NSInteger _origin_y;
}

- (id)initWithStakItem:(StakItem *)item inStakView:(StakView *)StakView;
- (void)textDidEndEditing:(NSNotification *)aNotification;
@property (readwrite, strong) NSTextView *content;
@property (readwrite, strong) StakView *stakView;
@property (readwrite, strong) NSImage *background;
@property (readwrite, strong) StakItem *item;

@end
