/**
 headstak - a stack for your head
 
 Copyright (C) 2012 dreadtech.com
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 **/
//
//  HSController.h
//  headstak
//
//  Created by Veghead on 7/24/11.

#import <Foundation/Foundation.h>

@class StakWindowController;
@class StakDB;
@class StakItem;
@class StakPrefController;

@interface StakViewController : NSViewController {

}

@property (strong, nonatomic) StakWindowController *swc;
@property (strong, nonatomic) StakPrefController *spc;

@property (strong, nonatomic) StakDB *db;
@property (nonatomic, strong) NSMutableArray *stakHolder;

- (void)setStak:(NSMutableArray *)stak;
- (void)layoutItemViews;
- (StakItem *)pushNewItemOfType:(NSUInteger)itemType;
- (StakItem *)popItem;
- (void)clearStakWarning;
- (void)exportStakSheet;

@end
