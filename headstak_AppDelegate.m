/**
 headstak - a stack for your head
 
 Copyright (C) 2012 dreadtech.com
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 **/

#import "headstak_AppDelegate.h"
#import <Carbon/Carbon.h>
#import "StakViewController.h"
#import "StakItem.h"
#import "StakView.h"
#import "StakWindowController.h"
#import "StakPrefController.h"

OSStatus hotKeyHandler(EventHandlerCallRef nextHandler, EventRef anEvent, void *userData);

@implementation headstak_AppDelegate

-(void)awakeFromNib {
    self.controller = [[StakViewController alloc] init];
    EventHotKeyRef hotKeyRef;
    EventHotKeyID hotKeyID;
    EventTypeSpec eventType;
    eventType.eventClass=kEventClassKeyboard;
    eventType.eventKind=kEventHotKeyPressed;
    EventHandlerUPP handler = NewEventHandlerUPP(hotKeyHandler);
    InstallApplicationEventHandler(handler,1,&eventType,(__bridge void *)self,NULL);
    hotKeyID.signature = 'vgHK';
    hotKeyID.id = 1;
    
    RegisterEventHotKey(29, cmdKey+controlKey, hotKeyID, GetApplicationEventTarget(), 0, &hotKeyRef);
    statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
    [statusItem setImage:[NSImage imageNamed:@"stakicon"]];
    [statusItem setMenu:[self stakMenu]];
    [statusItem setHighlightMode:YES];
    StakWindowController *swc = [[self controller] swc];
    for (StakItem *item in [[self controller] stakHolder]) {
        [[swc stakView] addSubview:(NSView *)[item viewInStakView:[swc stakView]]];
    }
    [self.window makeFirstResponder:[self.controller swc]];
}


- (IBAction)menuHandler:(id)sender {
    switch ([sender tag]) {
        //case (STAK_MENU_PREFS):
        //    [[[self hscontroller] spc] openStakWindow];
        //    break;
        case (STAK_MENU_CLEAR_HIST):
            [[self controller] clearStakWarning];
            break;
        case (STAK_MENU_EXPORT_HIST):
            [[self controller] exportStakSheet];
            break;
        // N.B. default also deals with a nil sender
        default:
        case (STAK_MENU_SHOW):
            [[[self controller] swc] openStakWindow];
            break;
    }
}


@end

OSStatus hotKeyHandler(EventHandlerCallRef nextHandler, EventRef theEvent, void *userData) {
    headstak_AppDelegate *ad = (__bridge headstak_AppDelegate *)userData;
    NSEvent *event = [NSEvent keyEventWithType:NSFlagsChanged location:NSZeroPoint
                                 modifierFlags:cmdKey
                                     timestamp:GetEventTime( theEvent )
                                  windowNumber:0 context:nil
                                    characters:nil charactersIgnoringModifiers:nil
                                     isARepeat:NO keyCode:0];
    [NSApp sendEvent:event];
    [ad menuHandler:nil];
    return noErr; 
}


