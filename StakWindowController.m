/**
 headstak - a stack for your head
 
 Copyright (C) 2012 dreadtech.com
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 **/
//
//  StakWindowController.m
//  headstak
//
//  Created by Veghead on 8/6/11.

#import "StakApplication.h"
#import "StakWindowController.h"
#import "StakView.h"
#import "StakItem.h"
#import "StakItemView.h"
#import "StakViewController.h"

enum {
    keycodeUp = 126,
    keycodeDown = 125,
    keycodeLeft = 123,
    keycodeRight = 124
};

@implementation StakWindowController


- (id)init {
    self = [super initWithWindowNibName:@"MainStak"];
    if (self) {
        [self window];
        // Initialization code here.
    }
    return self;
}


- (BOOL)windowShouldClose:(id)sender {
    return YES;
}


- (void)layoutItemViews {
    [[self controller] layoutItemViews];
}


- (void)windowDidLoad {
    [super windowDidLoad];
}

- (BOOL)acceptsFirstResponder {
    return YES;
}

- (BOOL)resignFirstResponder {
    return YES;
}

- (BOOL)becomeFirstResponder {
    return YES;
}

- (void)keyDown:(NSEvent *)theEvent {
    unsigned short key = [theEvent keyCode];
    LogDebug(@"Key: %u",key);

    StakItem *item;
    int itype = 1;
    switch(key) {
        // Pop
        case keycodeUp:
            item = [[self controller ] popItem];
            return;
        // Push
        case keycodeLeft:
            itype = 2;
            break;
        case keycodeRight:
            itype = 3;
            break;
        case keycodeDown:
        default:
            itype = 1;
            break;
    }
    item = [[self controller ] pushNewItemOfType:itype];
    [[self caption] setHidden:YES];
    [[self window] makeFirstResponder:[[item view] content]];
}

- (void)openStakWindow {
    [[self window] makeKeyAndOrderFront:self];
    [NSApp activateIgnoringOtherApps:YES];
    // Make the window first responder so it can 
    // deal with the command key
    [[self window] makeFirstResponder:[self window]];
    [[self caption] setHidden:NO];
    [self layoutItemViews];
}

- (void)windowHiding {
    // This should trigger the textview to announce it has been changed
    [[self window] makeFirstResponder:[self window]];
}

@end
