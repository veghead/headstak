/**
 headstak - a stack for your head
 
 Copyright (C) 2012 dreadtech.com
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 **/
//
//  headstakTests.m
//  headstakTests
//
//  Created by Veghead on 4/28/12.


#import "headstakTests.h"
#import "StakApplication.h"

@implementation headstakTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testHexColorConversion
{
    NSColor *shouldBeGreen = [StakApplication colourFromHexRGB:@"#abcdef"];
    STAssertNotNil(shouldBeGreen, @"failed to convert hex string with leading hash");
    shouldBeGreen = [StakApplication colourFromHexRGB:@"AbCdEf"];
    STAssertNotNil(shouldBeGreen, @"failed to convert hex string with mixed case");
    STAssertEqualsWithAccuracy([shouldBeGreen redComponent], 0xab / 255.0,0.001, @"hex colour conversion failed");
    STAssertEqualsWithAccuracy([shouldBeGreen greenComponent], 0xcd / 255.0,0.001, @"hex colour conversion failed");
    STAssertEqualsWithAccuracy([shouldBeGreen blueComponent], 0xef / 255.0,0.001, @"hex colour conversion failed");
}

@end
