/**
 headstak - a stack for your head
 
 Copyright (C) 2012 dreadtech.com
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 **/
//
//  StakItem.m
//  headstak
//
//  Created by Veghead on 3/11/12.


#import "StakItemView.h"
#import "StakView.h"
#import "StakItem.h"
#import "StakApplication.h"

@implementation StakItemView

- (id)initWithStakItem:(StakItem *)item inStakView:(StakView *)myStakView
{
    NSRect rect = [myStakView frame];
    rect.size.width -= 10;
    rect.origin.x += 5;
    rect.size.height = 40;
    self = [super initWithFrame:rect];
    if (self) {
        _item = item;
        _origin_y = rect.origin.y;
        _stakView = myStakView;
        
        // Make me look nice
        [self setBoxType:NSBoxCustom];
        [self setBorderType:NSLineBorder];
        [self setCornerRadius:10.0];
        
        NSColor *colour = [item colour];
        [self setFillColor:colour];
       
        rect.size.width -= 10;
        rect.origin.y = -10;
        
        // Create the Text View for the description
        _content = [[NSTextView alloc] initWithFrame:rect];
        [_content setBackgroundColor:[NSColor colorWithSRGBRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
        [_content insertText:[NSString stringWithString:[item itemDescription]]];
        [_content setEditable:YES];
        [_content setDelegate:self];
        // Observe changes to the container size so that we can adjust the view size
        [_content addObserver:self forKeyPath:@"textContainer.containerSize" options:NSKeyValueObservingOptionNew context:nil];

        [self addSubview:_content];
        _background = [NSImage imageNamed:@"taskbg"];
    }
    return self;
}

- (void)dealloc
{
    [_content removeObserver:self forKeyPath:@"textContainer.containerSize" context:nil];
}

- (BOOL)isFlipped
{
    return NO;
}

#pragma mark KVO

- (void)observeValueForKeyPath:(NSString *)keyPath 
                      ofObject:(id)object 
                        change:(NSDictionary *)change 
                       context:(id)context 
{
    NSRect textViewRect = [object frame];
    NSSize mySize = [self frame].size;
    mySize.height = textViewRect.size.height + 10;
    textViewRect.origin.y = 0;
    [[self content] setFrameOrigin:textViewRect.origin];
    [self setFrameSize:mySize];
    [[self stakView] itemChanged:self];
}

#pragma mark Gloss

- (void)drawRect:(NSRect)rect {
    [super drawRect:rect];
    NSRect r = [self bounds];
    r.origin.y = r.size.height - [[self background] size].height;
    [[self background] drawAtPoint:r.origin fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
}

- (void)textDidEndEditing:(NSNotification *)aNotification {
    [[self item] setItemDescription:[[self content] string]];
    [[self item] save];
}
@end
