//
//  StakPrefController.m
//  headstak
//
//  Created by Veghead on 6/7/12.
//  Copyright (c) 2012 dreadtech.com. All rights reserved.
//

#import "StakPrefController.h"

@implementation StakPrefController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}


- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (void)openStakWindow {
    [[self window] makeKeyAndOrderFront:self];
    [NSApp activateIgnoringOtherApps:YES];
    [[self window] makeFirstResponder:[self window]];
}

- (IBAction)close:(id)sender {
    [[self window] close];
}

@end
